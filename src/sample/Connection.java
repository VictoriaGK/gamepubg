package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Connection implements Runnable {
    private BufferedReader in;
    private PrintWriter out;
    private Socket client;
    int i;
    int j;
    long t;

    public long getT() {
        return t;
    }

    public void setT(long t) {
        this.t = t;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public Connection(Socket client) {
        this.client = client;
        try {
            this.in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            this.out = new PrintWriter(client.getOutputStream(),true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public BufferedReader getIn() {
        return in;
    }

    public void setIn(BufferedReader in) {
        this.in = in;
    }

    public PrintWriter getOut() {
        return out;
    }

    public void setOut(PrintWriter out) {
        this.out = out;
    }

    public Socket getClient() {
        return client;
    }

    public void setClient(Socket client) {
        this.client = client;
    }


    public void run() {


        while (true){
            for(Connection connection : Serv.clients){
                connection.getOut().println(getI());



            }

            for(Connection connection : Serv.clients){
                connection.getOut().println(getJ());


            }
            for(Connection connection : Serv.clients){
                connection.getOut().println(getT());
                System.out.println(getT()+"tttt");
            }
            try {

                String input = in.readLine();
                System.out.println(input+"jjj");
                if(input != null){
                    for(Connection connection : Serv.clients){
                        connection.getOut().println(input);
                    }
                }
                else{
                    Serv.clients.remove(this);
                    Thread.currentThread().stop();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

