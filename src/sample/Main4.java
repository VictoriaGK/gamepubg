package sample;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main4 extends Application {

    private final String HOST = "localhost";
    private final int PORT = 1234;
    int l;

    String status = "RIGHT";

    Pane root = new Pane();
    ArrayList<Player> players = new ArrayList<>();
    String aa;
    public String getAa() {
        return aa;
    }

    public void setAa(String aa) {
        this.aa = aa;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Socket s = new Socket(HOST, PORT);

        final String[] ripPers = {""};

        Listner2 listener2 = null;
        try {
            listener2 = new Listner2(new InputStreamReader(s.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert listener2 != null;
        Image iv2 = new Image(getClass().getResourceAsStream("naruto10.png"));
        ImageView iv = new ImageView(iv2);

        Player player = new Player(iv) ;


        Label label1 = new Label("MyPlayer hp: 100" );

        label1.setTranslateY(520);
        label1.setTranslateX(10);
        root.setBackground(new Background(new BackgroundFill(Color.NAVAJOWHITE, CornerRadii.EMPTY, Insets.EMPTY)));

        label1.toBack();



        root.getChildren().addAll(label1);

        root.getChildren().addAll(player);


        Listner2 finalListener = listener2;

        listener2.start();


        primaryStage.setScene(new Scene(root, 600, 550, Color.CORNSILK));


        final int[] i = {0};

        int index1;

        if(listener2.getInd().equals("")){
            index1 = 1;
        }else{
            index1 = Integer.parseInt(listener2.getInd());
        }

        int index = index1-1;
        int count;

        if(listener2.getStr().equals("")){
            count = 1;
        }else{
            count = Integer.parseInt(listener2.getStr());
        }

        int count2 = count-1;

        final int[] k = {0};



        Listner listner = new Listner(s.getOutputStream());

        if(listener2.isBye()){
            try {
                primaryStage.close();
                listener2.stop();

                new Main6().start(new Stage());
                return;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        AnimationTimer aaa = new AnimationTimer() {

            public void handle(long now) {

                if ((finalListener.getPlay())&&(i[0]==0)) {

                    if (((finalListener.getStr()).length() > 0) && (i[0] != Integer.parseInt(finalListener.getStr()))) {

                        if (finalListener.getStr().length() >= 4 || finalListener.getStr().equals("")) {
                            finalListener.setStr("0");
                        }


                        if ((!(finalListener.getStr().equals(""))) && (0 < finalListener.getStr().length())) {

                            for (int j = i[0]; j < (Integer.parseInt(finalListener.getStr())); j++) {

                                if (j == (index)) {
                                    players.add(player);

                                } else {
                                    Player player3 = finalListener.getPlayer();
                                    players.add(player3);
                                    root.getChildren().addAll(player3);
                                }

                            }
                        }
                        i[0] = Integer.parseInt(finalListener.getStr());
                        k[0] = i[0];

                    }
                }
            }

        };


        aaa.start();
        primaryStage.getScene().setOnKeyReleased(event -> {


            player.animation.stop();

        });


        primaryStage.getScene().setOnKeyPressed(event -> {

            if ((players.size() > 0) && (finalListener.getPlay())) {
                if (event.getCode() == KeyCode.DOWN) {

                    if (players.get(index).imageView.getY() <= 500) {
                        players.get(index).imageView.setY(players.get(index).imageView.getY() + 30);
                    }
                    players.get(index).animation.play();
                    players.get(index).animation.setOffsetY(0);


                    status = "DOWN";
                    listner.printWriter.println(status + index);
                    listner.printWriter.println((players.get(index).imageView.getY() + "hh" + (players.get(index).imageView.getX())));

                }
                if (event.getCode() == KeyCode.UP) {

                    if (players.get(index).imageView.getY() >= 0) {
                        players.get(index).imageView.setY(players.get(index).imageView.getY() - 30);
                    }
                    players.get(index).animation.play();
                    players.get(index).animation.setOffsetY(192);


                    status = "UP";
                    listner.printWriter.println(status + index);
                    listner.printWriter.println((players.get(index).imageView.getY() + "hh" + (players.get(index).imageView.getX())));

                }

                if (event.getCode() == (KeyCode.LEFT)) {
                    if (players.get(index).imageView.getX() >= 0) {
                        players.get(index).imageView.setX(players.get(index).imageView.getX() - 30);
                    }
                    players.get(index).animation.play();
                    players.get(index).animation.setOffsetY(64);
                    status = "LEFT";


                    listner.printWriter.println(status + index);
                    listner.printWriter.println((players.get(index).imageView.getY() + "hh" + (players.get(index).imageView.getX())));

                }
                if (event.getCode() == (KeyCode.RIGHT)) {
                    if (players.get(index).imageView.getX() <= 550) {
                        players.get(index).imageView.setX(players.get(index).imageView.getX() + 30);
                    }
                    players.get(index).animation.play();
                    players.get(index).animation.setOffsetY(128);
                    status = "RIGHT";
                    listner.printWriter.println(status + index);
                    listner.printWriter.println((players.get(index).imageView.getY() + "hh" + (players.get(index).imageView.getX())));

                }

            }


            if (event.getCode() == (KeyCode.ENTER)) {
                l++;

                Circle circle = new Circle(players.get(index).imageView.getX() + 23, players.get(index).imageView.getY() + 32, 5, Color.AQUA);

                root.getChildren().addAll(circle);
                Double param11 = circle.getCenterX();
                Double param22 = circle.getCenterY();

                Double param1 = circle.getCenterX();
                Double param2 = circle.getCenterY();
                for (Player player2 : players) {


                    if ((!player2.equals(players.get(index))) && (player2.imageView.getImage() != null)) {
                        switch (status) {
                            case "DOWN":

                                if (((player2.imageView.getX() - 50) <= players.get(index).imageView.getX()) && (players.get(index).imageView.getX() <= (player2.imageView.getX() + 50)) && (players.get(index).imageView.getY() < player2.imageView.getY()) && (player2.imageView.getY() > param22)) {
                                    param22 = player2.imageView.getY();

                                } else param2 = players.get(index).imageView.getY() + 500;


                                break;
                            case "UP":
                                if (((player2.imageView.getX() - 50) <= players.get(index).imageView.getX()) && (players.get(index).imageView.getX() <= (player2.imageView.getX() + 50)) && (players.get(index).imageView.getY() >= player2.imageView.getY()) && (player2.imageView.getY() < param22)) {
                                    param22 = player2.imageView.getY();


                                } else param2 = players.get(index).imageView.getY() - 500;
                                break;
                            case "LEFT":
                                if (((player2.imageView.getY() - 50) <= players.get(index).imageView.getY()) && (players.get(index).imageView.getY() <= (player2.imageView.getY() + 50)) && (players.get(index).imageView.getX() > player2.imageView.getX()) && (player2.imageView.getX() < param11)) {
                                    param11 = player2.imageView.getX();


                                } else param1 = players.get(index).imageView.getX() - 500;
                                break;
                            case "RIGHT":
                                if (((player2.imageView.getY() - 50) <= players.get(index).imageView.getY()) && (players.get(index).imageView.getY() <= (player2.imageView.getY() + 50)) && (players.get(index).imageView.getX() <= player2.imageView.getX()) && (player2.imageView.getX() > param11)) {
                                    param11 = player2.imageView.getX();

                                } else param1 = players.get(index).imageView.getX() + 500;
                                break;


                        }
                    }
                }

                if ((param11==circle.getCenterX())&&(param22==circle.getCenterY())) {
                    param22 = param2;
                    param11 = param1;
                }


                KeyValue kv = new KeyValue(circle.centerYProperty(), param22);
                KeyValue kv1 = new KeyValue(circle.centerXProperty(), param11);

                KeyFrame kf = new KeyFrame(Duration.seconds(1), kv, kv1);
                Timeline t = new Timeline(kf);

                t.play();
                t.setOnFinished(event2 -> {
                    circle.setVisible(false);


                });

                listner.printWriter.println("ENTER" + index + status + l);
            }

        });




        new AnimationTimer() {
            @Override
            public void handle(long now) {

                if ((players.size() > 1)&&(finalListener.getPlay())){

                    if((!(finalListener.getHp().equals(""))) &&(!(finalListener.getHp().equals(ripPers[0])))) {
                        players.get(Integer.parseInt(finalListener.getHp())).imageView.setImage(null);
                        k[0] = k[0] -1;
                        if((k[0]==1)&&(players.get(index).imageView.getImage()!=null)){
                            try {
                                primaryStage.close();
                                finalListener.stop();

                                new Main5().start(new Stage());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        ripPers[0] = finalListener.getHp();

                    }



                    String pat = ".*" + index + "{1}" + ".*";
                    String stat = finalListener.getStatus();
                    if (!(stat.equals(""))) {
                        Pattern pattern = Pattern.compile("\\d");
                        Matcher matcher = pattern.matcher(stat);
                        int tt = 0;
                        if (matcher.find()) {
                            tt = matcher.start();
                        };


                        char s3 = stat.charAt(tt);

                        String s5 = s3 + "";

                        int ind = Integer.parseInt(s5);

                        String XY = finalListener.getXY().split("hh")[0];
                        if (!(XY.equals(""))) {


                            String XY1 = finalListener.getXY().split("hh")[1];


                            if ((Pattern.matches("DOWN.+", stat)) && (!(Pattern.matches(pat, stat)))) {


                                if (players.get(ind).imageView.getY() <= 500) {

                                    players.get(ind).imageView.setY(Double.parseDouble(XY));

                                }


                                players.get(ind).animation.setOffsetY(0);
                                players.get(ind).animation.play();


                            }
                            if ((Pattern.matches("UP.+", stat)) && (!(Pattern.matches(pat, stat)))) {


                                if (players.get(ind).imageView.getY() >= 0) {

                                    players.get(ind).imageView.setY(Double.parseDouble(XY));

                                }
                                players.get(ind).animation.setOffsetY(192);
                                players.get(ind).animation.play();


                            }

                            if ((Pattern.matches("LEFT.+", stat)) && (!(Pattern.matches(pat, stat)))) {

                                if (players.get(ind).imageView.getX() >= 0) {

                                    players.get(ind).imageView.setX(Double.parseDouble(XY1));

                                }
                                players.get(ind).animation.setOffsetY(64);
                                players.get(ind).animation.play();


                            }

                            if ((Pattern.matches("RIGHT.+", stat)) && (!(Pattern.matches(pat, stat)))) {


                                if (players.get(ind).imageView.getX() <= 550) {

                                    players.get(ind).imageView.setX(Double.parseDouble(XY1));

                                }
                                players.get(ind).animation.setOffsetY(128);
                                players.get(ind).animation.play();


                            }
                        }



                        if ((Pattern.matches("ENTER.+", stat)) && (!(Pattern.matches(pat, stat)))) {
                            String status3 = finalListener.getStatus().substring(6);
                            if (!(status3.equals(getAa()))) {
                                Circle circle = new Circle(players.get(ind).imageView.getX() + 23, players.get(ind).imageView.getY() + 32, 5, Color.AQUA);

                                root.getChildren().addAll(circle);


                                Double param11 = circle.getCenterX();
                                Double param22 = circle.getCenterY();

                                Double param1 = circle.getCenterX();
                                Double param2 = circle.getCenterY();


                                for (Player player3 : players) {

                                    if ((!player3.equals(players.get(ind))) && (player3.imageView.getImage() != null)) {

                                        if (!(finalListener.getStatus().equals(""))) {

                                            String status2;


                                            status2 = status3.split("\\d")[0];

                                            switch (status2) {
                                                case "DOWN":

                                                    if (((player3.imageView.getX() - 50) <= players.get(ind).imageView.getX()) && (players.get(ind).imageView.getX() <= (player3.imageView.getX() + 50)) && (players.get(ind).imageView.getY() <= player3.imageView.getY()) && (player3.imageView.getY() > param22)) {
                                                        param22 = player3.imageView.getY();

                                                        if(player3.equals(players.get(index))){
                                                            players.get(index).setHp(players.get(index).getHp()-10);
                                                        }
                                                    } else param2 = players.get(ind).imageView.getY() + 500;


                                                    break;
                                                case "UP":
                                                    if (((player3.imageView.getX() - 50) <= players.get(ind).imageView.getX()) && (players.get(ind).imageView.getX() <= (player3.imageView.getX() + 50)) && (players.get(ind).imageView.getY() >= player3.imageView.getY()) && (player3.imageView.getY() < param22)) {
                                                        param22 = player3.imageView.getY();
                                                        if(player3.equals(players.get(index))){
                                                            players.get(index).setHp(players.get(index).getHp()-10);
                                                        }


                                                    } else param2 = players.get(ind).imageView.getY() - 500;
                                                    break;
                                                case "LEFT":
                                                    if (((player3.imageView.getY() - 50) <= players.get(ind).imageView.getY()) && (players.get(ind).imageView.getY() <= (player3.imageView.getY() + 50)) && (players.get(ind).imageView.getX() >= player3.imageView.getX()) && (player3.imageView.getX() < param11)) {
                                                        param11 = player3.imageView.getX();
                                                        if(player3.equals(players.get(index))){
                                                            players.get(index).setHp(players.get(index).getHp()-10);
                                                        }


                                                    } else param1 = players.get(ind).imageView.getX() - 500;
                                                    break;
                                                case "RIGHT":
                                                    if (((player3.imageView.getY() - 50) <= players.get(ind).imageView.getY()) && (players.get(ind).imageView.getY() <= (player3.imageView.getY() + 50)) && (players.get(ind).imageView.getX() <= player3.imageView.getX()) && (player3.imageView.getX() > param11)) {
                                                        param11 = player3.imageView.getX();
                                                        if(player3.equals(players.get(index))){
                                                            players.get(index).setHp(players.get(index).getHp()-10);
                                                        }

                                                    } else param1 = players.get(ind).imageView.getX() + 500;
                                                    break;


                                            }
                                        }

                                    }

                                }


                                if ((param11 == circle.getCenterX()) && (param22 == circle.getCenterY())) {
                                    param22 = param2;
                                    param11 = param1;
                                }

                                KeyValue kv = new KeyValue(circle.centerYProperty(), param22);

                                KeyValue kv1 = new KeyValue(circle.centerXProperty(), param11);


                                KeyFrame kf = new KeyFrame(Duration.seconds(1), kv, kv1);
                                Timeline t = new Timeline(kf);
                                Double finalParam = param11;
                                Double finalParam1 = param22;
                                t.play();
                                t.setOnFinished(event2 -> {
                                    circle.setVisible(false);
                                    if ((finalParam == players.get(index).imageView.getX()) || (finalParam1 == players.get(index).imageView.getY())) {
//
                                        label1.setText("MyPlayer hp:" + players.get(index).getHp());
//

                                    }


                                });

                                if (players.get(index).getHp() < 0) {
                                    listner.printWriter.println("Playerhpp:" + index);
                                    players.get(index).imageView.setImage(null);
                                    primaryStage.close();
                                    finalListener.stop();


                                    try {
                                        new Main3().start(new Stage());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }



                                }

                            }setAa(status3);


                        }



                    }


                }
            }


        }.start();


        primaryStage.setTitle("gamePubg");

        primaryStage.show();


    }

}

