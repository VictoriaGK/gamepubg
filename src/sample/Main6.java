package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main6 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        StackPane root = new StackPane();
        Label label = new Label("СЕРВЕР ПЕРЕПОЛНЕН, ОЖИДАЙТЕ");
        label.setRotate(40);

        label.setFont(new Font(30));
        root.getChildren().addAll(label);

        root.setBackground(new Background(new BackgroundFill(Color.BEIGE, CornerRadii.EMPTY, Insets.EMPTY)));

        primaryStage.setScene(new Scene(root, 600, 550, Color.CORNSILK));
        primaryStage.setTitle("Hello World");
        primaryStage.show();

    }
}
